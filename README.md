## How to run this code
- Run ```npm install``` or ```yarn``` if this is the first time you clone this repo (`master` branch).
- Run ```npm run start:frontend``` or ```yarn start:frontend``` to start this project in development mode.
- Sign in using username: `firstUser`, password: `example`
